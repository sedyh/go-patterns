package main

import "fmt"

type Command interface {
	Execute() string
}

type PowerOnCommand struct {
	receiver *Handler
}
func (c *PowerOnCommand) Execute() string {
	return c.receiver.PowerOn()
}

type PowerOffCommand struct {
	receiver *Handler
}
func (c *PowerOffCommand) Execute() string {
	return c.receiver.PowerOff()
}

type Handler struct {}
func (r *Handler) PowerOn() string {
	return "Toggle On"
}
func (r *Handler) PowerOff() string {
	return "Toggle Off"
}

type Caller struct {
	commands []Command
}

func (i *Caller) Push(command Command) {
	i.commands = append(i.commands, command)
}

func (i *Caller) Pop() {
	if len(i.commands) != 0 {
		i.commands = i.commands[:len(i.commands)-1]
	}
}

func (i *Caller) Execute() string {
	var result string
	for _, command := range i.commands {
		result += command.Execute() + "\n"
	}
	return result
}

func main() {
	handler := &Handler{}
	caller := &Caller{}
	caller.Push(&PowerOnCommand{handler})
	caller.Push(&PowerOffCommand{handler})
	fmt.Println(caller.Execute())
}