package main

import "fmt"

type Handler interface {
	Handle(msg int) string
}

type HandlerA struct {
	Next Handler
}

func (h *HandlerA) Handle(msg int) string {
	if msg == 1 {
		return "Handled by A"
	}
	if h.Next != nil {
		return h.Next.Handle(msg)
	}
	return ""
}

type HandlerB struct {
	Next Handler
}

func (h *HandlerB) Handle(msg int) string {
	if msg == 2 {
		return "Handled by B"
	}
	if h.Next != nil {
		return h.Next.Handle(msg)
	}
	return ""
}

type HandlerC struct {
	Next Handler
}

func (h *HandlerC) Handle(msg int) string {
	if msg == 3 {
		return "Handled by C"
	}
	if h.Next != nil {
		return h.Next.Handle(msg)
	}
	return ""
}

func main() {
	chain := &HandlerA{&HandlerB{&HandlerC{nil}}}
	for i:=0;i<4;i++ {
		fmt.Println(chain.Handle(i))
	}
}