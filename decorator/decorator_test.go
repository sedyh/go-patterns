package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type PhrasePrinter interface {
	Phrase() string
}

type WelcomePhrasePrinter struct {}
func NewWelcomePhrasePrinter() PhrasePrinter {
	return &WelcomePhrasePrinter{}
}
func (c *WelcomePhrasePrinter) Phrase() string {
	return "Good morning"
}

type QuoteDecorator struct {
	printer PhrasePrinter
}
func Wrap(printer PhrasePrinter) PhrasePrinter {
	return &QuoteDecorator{printer}
}
func (d *QuoteDecorator) Phrase() string {
	return "He said: '" + d.printer.Phrase() + "'"
}

func TestDecorator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Decorator")
}

var _ = Describe("Decorator", func() {
	It("Works properly", func() {
		printer := NewWelcomePhrasePrinter()
		Expect(printer.Phrase()).To(Equal("Good morning"))
		printer = Wrap(printer)
		Expect(printer.Phrase()).To(Equal("He said: 'Good morning'"))
	})
})
