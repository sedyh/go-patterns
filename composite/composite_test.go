package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Component interface {
	Add(child Component)
	Name() string
	Child() []Component
	Print(prefix string) string
}

type Directory struct {
	name   string
	childs []Component
}

func (d *Directory) Add(child Component) {
	d.childs = append(d.childs, child)
}

func (d *Directory) Name() string {
	return d.name
}

func (d *Directory) Child() []Component {
	return d.childs
}

func (d *Directory) Print(prefix string) string {
	result := prefix + "/" + d.Name() + "\n"
	for _, val := range d.Child() {
		result += val.Print(prefix + "/" + d.Name())
	}
	return result
}

func NewDirectory(name string) *Directory {
	return &Directory{
		name: name,
	}
}

func TestComposite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Composite")
}

var _ = Describe("Composite", func() {
	It("Works properly", func() {
		home := NewDirectory("home")
		sedyh := NewDirectory("sedyh")
		projects := NewDirectory("projects")
		desktop := NewDirectory("desktop")

		home.Add(sedyh)
		sedyh.Add(projects)
		projects.Add(NewDirectory("a"))
		projects.Add(NewDirectory("b"))
		projects.Add(desktop)
		desktop.Add(NewDirectory("a"))
		desktop.Add(NewDirectory("b"))

		expectProjects := ""+
			"/projects\n"+
			"/projects/a\n"+
			"/projects/b\n"+
			"/projects/desktop\n" +
			"/projects/desktop/a\n" +
			"/projects/desktop/b\n"

		expectDesktop := "" +
			"/desktop\n" +
			"/desktop/a\n" +
			"/desktop/b\n"

		Expect(projects.Print("")).To(Equal(expectProjects))
		Expect(desktop.Print("")).To(Equal(expectDesktop))
	})
})
