package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

const (
	MessageCallDispatcher = iota
	MessageAssignDriver
	MessageReturnCar
)

type Mediator interface {
	Notify(msg int)
}

type Passenger struct {
	mediator Mediator
	money, traffic int
}

func (p *Passenger) SetMediator(mediator Mediator) {
	p.mediator = mediator
}

func (p *Passenger) AddMoney(val int) {
	p.money += val
}

func (p *Passenger) GetMoney() int {
	return p.money
}

func (p *Passenger) CallDispatcher() {
	p.traffic -= 150
	p.mediator.Notify(MessageCallDispatcher)
}

type Dispatcher struct {
	mediator Mediator
	money, cars int
}

func (d *Dispatcher) SetMediator(mediator Mediator) {
	d.mediator = mediator
}

func (d *Dispatcher) GetMoney() int {
	return d.money
}

func (d *Dispatcher) AddMoney(val int) {
	d.money += val
}

func (d *Dispatcher) AddCars(val int) {
	d.cars += val
}

func (d *Dispatcher) AssignDriver() {
	d.cars--
	d.mediator.Notify(MessageAssignDriver)
}

type Driver struct {
	mediator Mediator
	money, fuel int
}

func (d *Driver) SetMediator(mediator Mediator) {
	d.mediator = mediator
}

func (d *Driver) AddMoney(val int) {
	d.money += val
}

func (d *Driver) GetMoney() int {
	return d.money
}

func (d *Driver) AddFuel(val int) {
	d.fuel += val
}

func (d *Driver) TransportPassenger() {
	d.fuel -= 1000
	d.mediator.Notify(MessageReturnCar)
}

type TaxiMediator struct {
	passenger *Passenger
	dispatcher *Dispatcher
	driver *Driver
}

func (t *TaxiMediator) Notify(msg int) {
	switch msg {
	case MessageCallDispatcher:
		t.passenger.AddMoney(-500)
		t.dispatcher.AddMoney(500)
		t.dispatcher.AssignDriver()
	case MessageAssignDriver:
		t.dispatcher.AddMoney(-250)
		t.driver.AddMoney(250) // Оплата за вычетом залога
		t.driver.AddFuel(890)
		t.driver.TransportPassenger()
	case MessageReturnCar:
		t.dispatcher.AddCars(1)
		t.dispatcher.AddMoney(-100)
		t.driver.AddMoney(100) // Залог минус расход на топливо
	}
}

func TestMediator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Mediator")
}

var _ = Describe("Mediator", func() {
	It("Works properly", func() {
		passenger := &Passenger{
			money:    15000,
			traffic:  450,
		}
		dispatcher := &Dispatcher{
			money:    13053000,
			cars:     24,
		}
		driver := &Driver{
			money:    24000,
			fuel:     0,
		}

		mediator := &TaxiMediator{
			passenger:  passenger,
			dispatcher: dispatcher,
			driver:     driver,
		}
		passenger.SetMediator(mediator)
		dispatcher.SetMediator(mediator)
		driver.SetMediator(mediator)

		passenger.CallDispatcher()

		Expect(passenger.GetMoney()).To(Equal(14500))
		Expect(dispatcher.GetMoney()).To(Equal(13053150))
		Expect(driver.GetMoney()).To(Equal(24350))
	})
})
