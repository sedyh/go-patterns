## Observer/Наблюдатель

Поведенческий паттерн, который позволяет одним объектам следить
и реагировать на события, происходящие в других объектах.

Когда после изменения состояния одного объекта требуется
что-то сделать в других, но вы не знаете наперёд,
какие именно объекты должны отреагировать.

Когда одни объекты должны наблюдать за другими,
но только в определённых случаях.

Имеется два способа получения уведомлений от издателя:

Метод вытягивания: После получения уведомления от издателя, подписчик должен пойти к издателю и забрать (вытянуть) данные самостоятельно.
Метод проталкивания: Издатель не уведомляет подписчика об обновлениях данных, а самостоятельно доставляет (проталкивает) данные подписчику.
