package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Publisher interface {
	Connect(observer Observer)
	SetState(state string)
	Notify()
}

type Observer interface {
	Update(state string)
}

type ConcretePublisher struct {
	observers []Observer
	state     string
}

func NewPublisher() Publisher {
	return &ConcretePublisher{}
}

func (s *ConcretePublisher) Connect(observer Observer) {
	s.observers = append(s.observers, observer)
}

func (s *ConcretePublisher) SetState(state string) {
	s.state = state
}

func (s *ConcretePublisher) Notify() {
	for _, observer := range s.observers {
		observer.Update(s.state)
	}
}

type ConcreteObserver struct {
	state string
}

func (s *ConcreteObserver) Update(state string) {
	s.state = state
}

func TestMediator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Mediator")
}

var _ = Describe("Mediator", func() {
	It("Works properly", func() {
		stateA := "StateA"
		stateB := "StateB"
		stateC := "StateC"

		publisher := NewPublisher()

		observerA := &ConcreteObserver{}
		observerB := &ConcreteObserver{}
		publisher.Connect(observerA)
		publisher.Connect(observerB)

		publisher.SetState(stateA)
		publisher.Notify()

		Expect(observerA.state).To(Equal(stateA))
		Expect(observerB.state).To(Equal(stateA))

		publisher.SetState(stateB)
		publisher.Notify()

		Expect(observerA.state).To(Equal(stateB))
		Expect(observerB.state).To(Equal(stateB))

		publisher.SetState(stateC)
		publisher.Notify()

		Expect(observerA.state).To(Equal(stateC))
		Expect(observerB.state).To(Equal(stateC))
	})
})
