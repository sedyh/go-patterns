package main

import (
	"fmt"
	"math"
)

type Component interface {
	Mask() uint64
}

type Entity interface {
	Add(c ...Component)
	Get(mask uint64) Component
	ID() string
	Mask() uint64
	Remove(mask uint64)
}

type entity struct {
	Components []Component
	Id         string
	Masked     uint64
}

func NewEntity(id string, components ...Component) Entity {
	return &entity{
		Components: components,
		Id:         id,
		Masked:     maskSlice(components),
	}
}

func (e *entity) Add(cn ...Component) {
	for _, c := range cn {
		if e.Masked&c.Mask() == c.Mask() {
			continue
		}
		e.Components = append(e.Components, c)
		e.Masked = maskSlice(e.Components)
	}
}

func (e *entity) Get(mask uint64) Component {
	for _, c := range e.Components {
		if c.Mask() == mask {
			return c
		}
	}
	return nil
}

func (e *entity) ID() string {
	return e.Id
}

func (e *entity) Mask() uint64 {
	return e.Masked
}

func (e *entity) Remove(mask uint64) {
	modified := false
	for i, c := range e.Components {
		if c.Mask() == mask {
			copy(e.Components[i:], e.Components[i+1:])
			e.Components[len(e.Components)-1] = nil
			e.Components = e.Components[:len(e.Components)-1]
			modified = true
			break
		}
	}
	if modified {
		e.Masked = maskSlice(e.Components)
	}
}

func maskSlice(components []Component) uint64 {
	mask := uint64(0)
	for _, c := range components {
		mask = mask | c.Mask()
	}
	return mask
}

type EntityManager interface {
	Add(entities ...Entity)
	Entities() (entities []Entity)
	FilterByMask(mask uint64) (entities []Entity)
	GetByID(id string) (entity Entity)
	Remove(entity Entity)
}

type entityManager struct {
	entities []Entity
	index    []int
}

func NewEntityManager() EntityManager {
	return &entityManager{
		entities: []Entity{},
	}
}

func (m *entityManager) Add(entities ...Entity) {
	for _, entity := range entities {
		m.entities = append(m.entities, entity)
	}
}

func (m *entityManager) Entities() (entities []Entity) {
	return m.entities
}

func (m *entityManager) FilterByMask(mask uint64) (entities []Entity) {
	entities = make([]Entity, len(m.entities))
	index := 0
	for _, e := range m.entities {
		observed := e.Mask()
		if observed&mask == mask {
			entities[index] = e
			index++
		}
	}
	return entities[:index]
}

func (m *entityManager) GetByID(id string) (entity Entity) {
	for _, e := range m.entities {
		if e.ID() == id {
			return e
		}
	}
	return
}

func (m *entityManager) Remove(entity Entity) {
	for i, e := range m.entities {
		if e.ID() == entity.ID() {
			copy(m.entities[i:], m.entities[i+1:])
			m.entities[len(m.entities)-1] = nil
			m.entities = m.entities[:len(m.entities)-1]
			break
		}
	}
}

type System interface {
	Update(entities EntityManager)
}

type SystemManager interface {
	Add(systems ...System)
	Systems() []System
}

type systemManager struct {
	systems []System
}

func NewSystemManager() SystemManager {
	return &systemManager{[]System{}}
}

func (m *systemManager) Add(systems ...System) {
	for _, system := range systems {
		m.systems = append(m.systems, system)
	}
}

func (m *systemManager) Systems() []System {
	return m.systems
}

func IdGenerator() func() string {
	start := uint64(0)
	return func() string {
		start = (start + 1) % (math.MaxUint64 - 1)
		return fmt.Sprintf("%d", start)
	}
}

func MaskGenerator() func() uint64 {
	start := 0
	return func() uint64 {
		mask := uint64(1 << start)
		start++
		return mask
	}
}

var (
	NewComponentMask = MaskGenerator()
	NewEntityId = IdGenerator()
)

var (
	NameMask = NewComponentMask()
	CarMask = NewComponentMask()
	PhoneMask = NewComponentMask()
)

type Name struct {
	Data string
}
func (n *Name) Mask() uint64 {
	return NameMask
}

type Phone struct {
	Data string
}
func (p *Phone) Mask() uint64 {
	return PhoneMask
}

type Car struct {
	Data string
}
func (c *Car) Mask() uint64 {
	return CarMask
}

type CallingSystem struct {}
func (s *CallingSystem) Update(manager EntityManager) {
	for _, client := range manager.FilterByMask(NameMask|PhoneMask) {
		name := client.Get(NameMask).(*Name).Data
		phone := client.Get(PhoneMask).(*Phone).Data
		fmt.Println("client", name, "calling to", phone)
	}
}

type DrivingSystem struct {}
func (s *DrivingSystem) Update(manager EntityManager) {
	for _, client := range manager.FilterByMask(NameMask|CarMask) {
		name := client.Get(NameMask).(*Name).Data
		car := client.Get(CarMask).(*Car).Data
		fmt.Println("driver", name, "transfer client on car", car)
	}
}

func main() {
	systems := NewSystemManager()
	systems.Add(&CallingSystem{})
	systems.Add(&DrivingSystem{})

	entities := NewEntityManager()
	for i:=0; i<10; i++ {
		entities.Add(NewEntity(
			NewEntityId(),
			&Name{},
			&Phone{},
		))
		entities.Add(NewEntity(
			NewEntityId(),
			&Name{},
			&Car{},
		))
	}

	for _, system := range systems.Systems() {
		system.Update(entities)
	}
}