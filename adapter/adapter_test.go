package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type EnglishPlug interface {
	Connect() string
}

type EuropeanPlug struct {}
func (a *EuropeanPlug) Connect() string {
	return "Connected"
}

type Adapter struct {
	plug *EuropeanPlug
}
func NewAdapter(plug *EuropeanPlug) EnglishPlug {
	return &Adapter{plug}
}
func (a *Adapter) Connect() string {
	return a.plug.Connect()
}

func TestAdapter(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Adapter")
}

var _ = Describe("Adapter", func() {
	It("Works properly", func() {
		adapter := NewAdapter(&EuropeanPlug{})
		msg := adapter.Connect()
		Expect(msg).To(Equal("Connected"))
	})
})
