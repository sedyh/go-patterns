package main

import (
	"fmt"
)

type Visitor interface {
	VisitTriangle(t *Triangle)
	VisitSquare(s *Square)
	VisitPentagon(p *Pentagon)
}

type Visitable interface {
	Accept(v Visitor)
}

type AngleCountExporter struct {}
func (a *AngleCountExporter) VisitTriangle(t *Triangle) {
	fmt.Println("Exported angles of triangle:", t.AnglesCount)
}
func (a *AngleCountExporter) VisitSquare(s *Square) {
	fmt.Println("Exported angles of square:", s.AnglesCount)
}
func (a *AngleCountExporter) VisitPentagon(p *Pentagon) {
	fmt.Println("Exported angles of pentagon:", p.AnglesCount)
}

type DegreeSumExporter struct {}
func (a *DegreeSumExporter) VisitTriangle(t *Triangle) {
	fmt.Println("Exported degrees sum of triangle:", t.DegreeSum)
}
func (a *DegreeSumExporter) VisitSquare(s *Square) {
	fmt.Println("Exported degrees sum of square:", s.DegreeSum)
}
func (a *DegreeSumExporter) VisitPentagon(p *Pentagon) {
	fmt.Println("Exported degrees sum of pentagon:", p.DegreeSum)
}

type Triangle struct {
	AnglesCount int
	DegreeSum int
}
func NewTriangle() *Triangle {
	return &Triangle{
		AnglesCount: 3,
		DegreeSum:   180,
	}
}
func (t *Triangle) Accept(v Visitor) {
	v.VisitTriangle(t)
}

type Square struct {
	AnglesCount int
	DegreeSum int
}
func NewSquare() *Triangle {
	return &Triangle{
		AnglesCount: 4,
		DegreeSum:   360,
	}
}
func (s *Square) Accept(v Visitor) {
	v.VisitSquare(s)
}

type Pentagon struct {
	AnglesCount int
	DegreeSum int
}
func NewPentagon() *Triangle {
	return &Triangle{
		AnglesCount: 5,
		DegreeSum:   540,
	}
}
func (p *Pentagon) Accept(v Visitor) {
	v.VisitPentagon(p)
}

func main() {
	visitables := []Visitable{
		NewTriangle(),
		NewSquare(),
		NewPentagon(),
	}
	visitors := []Visitor{
		&AngleCountExporter{},
		&DegreeSumExporter{},
	}
	for _, visitor := range visitors {
		for _, visitable := range visitables {
			visitable.Accept(visitor)
		}
	}
}
