package main

import "fmt"

type QuotesInterface interface {
	Open() string
	Close() string
}

type Quotes struct {
	QuotesInterface
}
func (q *Quotes) Quotes(str string) string {
	return q.Open() + str + q.Close()
}
func NewQuotes(qt QuotesInterface) *Quotes {
	return &Quotes{qt}
}

type MysqlQuote struct {}
func (q *MysqlQuote) Open() string {
	return "`"
}
func (q *MysqlQuote) Close() string {
	return "`"
}

type PostgresQuote struct {}
func (q *PostgresQuote) Open() string {
	return "\""
}
func (q *PostgresQuote) Close() string {
	return "\""
}

func main() {
	quotes := []*Quotes{
		NewQuotes(&MysqlQuote{}),
		NewQuotes(&PostgresQuote{}),
	}
	for _, q := range quotes {
		fmt.Println(q.Quotes("complex_field"))
	}
}