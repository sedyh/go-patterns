package main

import (
	"strings"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Remote struct {
	monitor *Monitor
	lights  *Lights
	audio *Audio
}
func NewRemote() *Remote {
	return &Remote{
		monitor: &Monitor{},
		lights:  &Lights{},
		audio:   &Audio{},
	}
}

func (m *Remote) StartMovie() string {
	return strings.Join([]string{
		m.monitor.Start(),
		m.lights.Dim(),
		m.audio.Play(),
	}, "\n")
}

type Monitor struct {}
func (m *Monitor) Start() string {
	return "Turn on the monitor"
}

type Lights struct {}
func (l *Lights) Dim() string {
	return "Dimmed the lights"
}

type Audio struct {}
func (a *Audio) Play() string {
	return "Play the audio"
}

func TestFacade(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Facade")
}

var _ = Describe("Facade", func() {
	It("Works properly", func() {
		remote := NewRemote()
		result := remote.StartMovie()
		Expect(result).To(Equal(""+
			"Turn on the monitor\n"+
			"Dimmed the lights\n"+
			"Play the audio",
		))
	})
})
