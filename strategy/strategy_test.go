package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Sorter interface {
	Sort(arr []int) []int
}

type User struct {
	strategy Sorter
}

func (u *User) Method(s Sorter) {
	u.strategy = s
}

func (u *User) Sort(arr []int) []int {
	return u.strategy.Sort(arr)
}

type MergeSort struct {}

func (m *MergeSort) Sort(arr []int) []int {
	l := len(arr)
	if l < 2 {
		return arr
	}

	pivot := l / 2
	return m.MergeSortedSets(m.Sort(arr[:pivot]), m.Sort(arr[pivot:]))
}

func (m *MergeSort) MergeSortedSets(a, b []int) []int {
	l := len(a) + len(b)
	group := make([]int, l)
	aPos := 0
	bPos := 0
	for j := 0; j < l; j++ {
		if aPos == len(a) {
			group[j] = b[bPos]
			bPos++
		} else if bPos == len(b) {
			group[j] = a[aPos]
			aPos++
		} else if a[aPos] <= b[bPos] {
			group[j] = a[aPos]
			aPos++
		} else {
			group[j] = b[bPos]
			bPos++
		}
	}
	return group
}

type QuickSort struct {}

func (q *QuickSort) Sort(s []int) []int {
	q.Quicksort(&s, 0, len(s)-1)
	return s
}

func (q *QuickSort) Quicksort(s *[]int, lo, hi int) {
	if lo < hi {
		p := partition(s, lo, hi)
		q.Quicksort(s, lo, p)
		q.Quicksort(s, p+1, hi)
	}
}

func partition(s *[]int, lo, hi int) int {
	pivot := (*s)[lo]
	i := lo - 1
	if i == -1 {
		i++
	}
	j := hi + 1
	if j == len(*s) {
		j--
	}
	for {
		for (*s)[i] < pivot {
			i++
		}
		for (*s)[j] > pivot {
			j--
		}
		if i >= j {
			return j
		}
		(*s)[i], (*s)[j] = (*s)[j], (*s)[i]
	}
}

func TestStrategy(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Strategy")
}

var _ = Describe("Strategy", func() {
	It("Works properly", func() {
		expecting := []int{-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
		dataA := []int{8, 9, 4, 2, 5, 7, 6, 3, 1, -2, -1, 0, -3}
		dataB := []int{-3, 5, 1, 7, 4, -1, 8, 2, 0, 9, 6, 3, -2}
		
		user := &User{}
		user.Method(&MergeSort{})
		actualA := user.Sort(dataA)
		user.Method(&QuickSort{})
		actualB := user.Sort(dataB)
		
		Expect(actualA).To(Equal(expecting))
		Expect(actualB).To(Equal(expecting))
	})
})
