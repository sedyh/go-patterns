package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Builder interface {
	MakeRoof(hardened bool)
	MakeFloor(hardened bool)
	MakeBasement(hardened bool)
}

type Director struct {
	builder Builder
}

func (d *Director) ConstructSkyscrapper() {
	d.builder.MakeBasement(true)
	for i:=0;i<23;i++ {
		d.builder.MakeFloor(i%4==3)
	}
	d.builder.MakeRoof(true)
}

func (d *Director) ConstructHotel() {
	d.builder.MakeBasement(true)
	for i:=0;i<5;i++ {
		d.builder.MakeFloor(false)
	}
	d.builder.MakeRoof(true)
}

func (d *Director) ConstructHouse() {
	d.builder.MakeBasement(false)
	d.builder.MakeFloor(false)
	d.builder.MakeRoof(false)
}

type BuildingBuilder struct {
	product *Product
}

func (b *BuildingBuilder) MakeRoof(hardened bool) {
	if hardened {
		b.product.Content += "|"
	}
	b.product.Content += "▙"
}

func (b *BuildingBuilder) MakeFloor(hardened bool) {
	if hardened {
		b.product.Content += "|"
	}
	b.product.Content += "▞"
}

func (b *BuildingBuilder) MakeBasement(hardened bool) {
	b.product.Content += "█"
	if hardened {
		b.product.Content += "|"
	}
}

type Product struct {
	Content string
}

func (p *Product) Show() string {
	return p.Content
}

func (p *Product) Clear() {
	p.Content = ""
}

func TestBuilder(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Builder")
}

var _ = Describe("Builder", func() {
	It("Works properly", func() {
		product := &Product{}
		director := &Director{&BuildingBuilder{product}}

		director.ConstructHouse()
		Expect(product.Show()).To(Equal("█▞▙"))
		product.Clear()

		director.ConstructHotel()
		Expect(product.Show()).To(Equal("█|▞▞▞▞▞|▙"))
		product.Clear()

		director.ConstructSkyscrapper()
		Expect(product.Show()).To(Equal("█|▞▞▞|▞▞▞▞|▞▞▞▞|▞▞▞▞|▞▞▞▞|▞▞▞▞|▙"))
		product.Clear()
	})
})
