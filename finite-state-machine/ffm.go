package main

import "fmt"

const (
	StartState = iota
	AState
	BState
	CState
	FinalState
)

var ctx = 0

func DoStart(ctx int) int{
	fmt.Println("start")
	switch ctx {
	case 1:
		return AState
	default:
		return CState
	}
}

func DoA(ctx int) int {
	fmt.Println("a")
	switch ctx {
	case 2:
		return CState
	case 4:
		return AState
	default:
		return BState
	}
}

func DoB(ctx int) int {
	fmt.Println("b")
	switch ctx {
	case 5:
		return AState
	default:
		return CState
	}
}

func DoC(ctx int) int {
	fmt.Println("c")
	switch ctx {
	case 11:
		return FinalState
	default:
		return BState
	}
}

func main() {
	state := StartState
	for {
		ctx++
		switch state {
		case StartState:
			state = DoStart(ctx)
		case AState:
			state = DoA(ctx)
		case BState:
			state = DoB(ctx)
		case CState:
			state = DoC(ctx)
		case FinalState:
			fmt.Println("end")
			return
		}
	}
}

