package main

import "fmt"

type Transition struct {
	To string
}
type Transitions map[string]Transition

type State struct {
	On Transitions
}
type States map[string]State

type Machine struct {
	Start string
	Current string
	States States
}

func (m *Machine) Transition(event string) string {
	current := m.Current
	if current == "" {
		current = m.Start
	}
	transitions := m.States[current].On
	next := transitions[event].To
	if next != "" {
		m.Start = next
		return next
	}
	return current
}

func main() {
	const (
		SpeedMin = "SPEED_MIN"
		SpeedLow = "SPEED_LOW"
		SpeedMid = "SPEED_MID"
		SpeedHigh = "SPEED_HIGH"
	)
	const (
		Increase = "INCREASE"
		Decrease = "DECREASE"
		Stop = "STOP"
	)
	machine := Machine{
		Start:   SpeedMin,
		States:  States{
			SpeedMin: State{
				Transitions{
					Increase: Transition{
						To: SpeedLow,
					},
					Decrease: Transition{
						To: SpeedMin,
					},
					Stop: Transition{
						To: SpeedMin,
					},
				},
			},
			SpeedLow: State{
				Transitions{
					Increase: Transition{
						To: SpeedMid,
					},
					Decrease: Transition{
						To: SpeedMin,
					},
					Stop: Transition{
						To: SpeedMin,
					},
				},
			},
			SpeedMid: State{
				Transitions{
					Increase: Transition{
						To: SpeedHigh,
					},
					Decrease: Transition{
						To: SpeedLow,
					},
					Stop: Transition{
						To: SpeedMin,
					},
				},
			},
			SpeedHigh: State{
				Transitions{
					Increase: Transition{
						To: SpeedHigh,
					},
					Decrease: Transition{
						To: SpeedMid,
					},
					Stop: Transition{
						To: SpeedMin,
					},
				},
			},
		},
	}
	fmt.Println(machine.Transition(Stop))
	fmt.Println(machine.Transition(Increase))
	fmt.Println(machine.Transition(Increase))
	fmt.Println(machine.Transition(Increase))
	fmt.Println(machine.Transition(Decrease))
	fmt.Println(machine.Transition(Increase))
	fmt.Println(machine.Transition(Stop))
}