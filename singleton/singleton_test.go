package main

import (
	"sync"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Singleton struct {}

var (
	instance *Singleton
	once     sync.Once
)

func Instance() *Singleton {
	once.Do(func() {
		instance = &Singleton{}
	})
	return instance
}

func TestSingleton(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Singleton")
}

var _ = Describe("Singleton", func() {
	It("Works properly", func() {
		instanceA := Instance()
		instanceB := Instance()
		Expect(instanceA).To(Equal(instanceB))
	})
})
