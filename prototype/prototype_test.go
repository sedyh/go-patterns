package main

import (
	"fmt"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type Prototyper interface {
	Clone() Prototyper
	Config() string
}

type Connection struct {
	environmentVarA string
	environmentVarB string
	environmentVarC string
}

func NewConnection(
	environmentVarA string,
	environmentVarB string,
	environmentVarC string,
) Prototyper {
	return &Connection{
		environmentVarA: environmentVarA,
		environmentVarB: environmentVarB,
		environmentVarC: environmentVarC,
	}
}

func (p *Connection) Config() string {
	return fmt.Sprintf(
		"db://%s:%s:%s",
		p.environmentVarA,
		p.environmentVarB,
		p.environmentVarC,
	)
}

func (p *Connection) Clone() Prototyper {
	return &Connection{
		p.environmentVarA,
		p.environmentVarB,
		p.environmentVarC,
	}
}

func TestPrototype(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Prototype")
}

var _ = Describe("Prototype", func() {
	It("Works properly", func() {
		con := NewConnection("url", "login", "pass")
		another := con.Clone()
		Expect(another.Config()).To(Equal("db://url:login:pass"))
	})
})
